<?php

/**
 * Не обращай внимания на то, что все классы в одном файле,
 * это черновой вариант и я пока не искал как в компоненте по разным файлам все классы разнести.
 */

defined('B_PROLOG_INCLUDED') || die;


use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\UI\Extension;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

// В класс с константами все поля в идеале должны приходить из настроек компонента.
class Constants {
    const SECTION_ID_VELESSTROY = 1; // id главного подразделения.
    const CHAT_BOTS_ID = 3873; // id подразделения "Чат-боты" для исключения.
    const BP = 33; // id БП "Выдача справки"
    const CERT_TYPE_NDFL = 71; // Тип справки - 2-НДФЛ;
    const CERT_TYPE_NDFL_TAX = 72; // Тип справки - 2-НДФЛ для налоговой;
    const CERT_TYPE_ALIMONY = 75; // Тип справки - алименты.
    const CERT_TYPE_NOT_GET_ALLOWANCE_NOT_VACATION = 73; // Тип справки - дети.
    const CERT_TYPE_NOT_GET_ALLOWANCE = 74; // Тип справки - дети.
    const CERT_FORM_PAPER = 82; // Форма справки - бумажная.
    const CERT_FORM_ELECTRONIC = 81; // Форма справки - электронная.
    const DELIVERY_METHOD_MAIL = 84; // Способ доставки - почта.
    const GROUP_OPERATORS = 28; // Группа - Операторы единого окна.
    const GROUP_ADMINS_UNIWIN = 29; // Группа - Операторы единого окна.
    const INPUT_SOURCE_UNIWIN = 90; // Источник входа - единое окно.
    const INPUT_SOURCE_PORTAL = 88; // Источник входа - портал.
    const INPUT_SOURCE_SITE = 89; // Источник входа - сайт.
    const STATUS_REQUEST_OPEN = 91; // Статус - заявка открыта.
}

class UserInfo {
    private $source;
    private $initiator;

    public function __construct(CUser $employee, ?string $initiator = null) {
        if (CSite::InGroup(array(Constants::GROUP_OPERATORS, Constants::GROUP_ADMINS_UNIWIN))) {
            $this->source = Constants::INPUT_SOURCE_UNIWIN;
            $this->initiator = $initiator;
        } elseif ($employee->IsAuthorized()) {
            $this->source = Constants::INPUT_SOURCE_PORTAL;
            $this->initiator = $employee->GetID();
        } else {
            $this->source = Constants::INPUT_SOURCE_SITE;
        }
    }

    public function getSource() {
        return $this->source;
    }

    public function getInitiatorId() {
        return $this->initiator;
    }
}

class Structure {
    private $idHead;
    public $legalEntity = '';
    public $separateSubdivision = '';

    public function __construct($idHead) {
        $this->idHead = $idHead;
    }

    public function searchDepartments($id) {
        $sections = CIBlockSection::GetList(
            array(),
            array('ID' => $id),
            false,
            array('ID', 'IBLOCK_SECTION_ID', 'NAME')
        );
        if ($item = $sections->Fetch()) {
            if ($item['IBLOCK_SECTION_ID'] == Constants::SECTION_ID_VELESSTROY) {
                return;
            }
            $this->legalEntity = $item['IBLOCK_SECTION_ID'];
            $this->separateSubdivision = $item['ID'];
            $this->searchDepartments($this->legalEntity);
        }
    }
}

class ResultBuilder {
    private $fields = array();

    public function createForUser(UserInfo $user): self {
        $this->addInputSource($user->getSource());

        if ($user->getSource() !== Constants::INPUT_SOURCE_SITE) {
            $this->addEmployeeInfo($user->getInitiatorId());
        }

        if ($user->getSource() === Constants::INPUT_SOURCE_PORTAL) {
            $this->addBlockedFlags();
        }

        return $this;
    }

    public function withFieldsFromBp($id): self {
        $fieldsResult = CIBlock::GetProperties($id);

        while ($field = $fieldsResult->Fetch()) {
            $this->fields[$field['CODE']]['code'] = $field['CODE'];
            $this->fields[$field['CODE']]['name'] = $field['NAME'];
            $this->fields[$field['CODE']]['value'] = $this->fields[$field['CODE']]['value'] ?? '';
            if ($field['LIST_TYPE'] === 'L') {
                $fResult =  CIBlockProperty::GetPropertyEnum($field['ID']);
                while ($value = $fResult->Fetch()) {
                    $this->fields[$field['CODE']]['list_values'][$value['ID']] = $value['VALUE'];
                }
            }
        }
        $this->fields['CERT_FORM']['value'] = 81;

        return $this;
    }

    public function withStructureCompany(Structure $structure): self {
        // Получение структуры компании 1 уровня.
        $velesstroySections1level = CIBlockSection::GetList(
            array(),
            array(
                'SECTION_ID' => Constants::SECTION_ID_VELESSTROY,
                'INCLUDE_SUBSECTIONS' => 'Y'
            ),
            false,
            array('ID', 'NAME')
        );
        while ($velesstroySectionItem = $velesstroySections1level->Fetch()) {
            if ($velesstroySectionItem['ID'] == Constants::CHAT_BOTS_ID) {
                continue;
            }

            $this->fields['UL_NAME']['list_values'][$velesstroySectionItem['ID']]['name'] = $velesstroySectionItem['NAME'];
        }

        // Получение id подразделений 2 уровня.
        $idSections2Level = array();
        foreach ($this->fields['UL_NAME']['list_values'] as $idLegalEntity => $nameLegalEntity) {
            $idSections2Level[] = $idLegalEntity;
        }

        // Получение структуры компании 2 уровня.
        $velesstroySections2level = CIBlockSection::GetList(
            array(),
            array(
                'SECTION_ID' => $idSections2Level,
                'INCLUDE_SUBSECTIONS' => 'Y'
            ),
            false,
            array('ID', 'NAME', 'IBLOCK_SECTION_ID')
        );
        while ($item = $velesstroySections2level->Fetch()) {
            $this->fields['UL_NAME']['list_values'][$item['IBLOCK_SECTION_ID']]['subdivisions'][$item['ID']] = $item['NAME'];
        }

        return $this;
    }

    public function withValidateData(): self {
        // ['type'] - тип инпута (если поле должно быть инпутом).
        // ['pattern'] - регулярное выражение для проверки поля.
        // ['errortext'] - текст ошибки для вывода сообщения если валидация не прошла.
        // ['required'] - true, поле обязательно.
        // ['DEPENDENCE'][id] = array(field_names) - если значение поля = id, то поля field_names обязательны.

        $this->fields['CERT_TYPE']['pattern'] = '^.+$';
        $this->fields['CERT_TYPE']['errortext'] = ' Заполните поле';
        $this->fields['CERT_TYPE']['required'] = true;
        $this->fields['CERT_TYPE']['DEPENDENCE'][Constants::CERT_TYPE_NDFL] = array('DATE_START_PERIOD', 'DATE_END_PERIOD');
        $this->fields['CERT_TYPE']['DEPENDENCE'][Constants::CERT_TYPE_NDFL_TAX] = array('DATE_START_PERIOD', 'DATE_END_PERIOD');
        $this->fields['CERT_TYPE']['DEPENDENCE'][Constants::CERT_TYPE_ALIMONY] = array('DATE_START_PERIOD', 'DATE_END_PERIOD');
        $this->fields['CERT_TYPE']['DEPENDENCE'][Constants::CERT_TYPE_NOT_GET_ALLOWANCE] = array('FULL_NAME_CHILD', 'BIRTHDATE_CHILD');
        $this->fields['CERT_TYPE']['DEPENDENCE'][Constants::CERT_TYPE_NOT_GET_ALLOWANCE_NOT_VACATION] = array('FULL_NAME_CHILD', 'BIRTHDATE_CHILD');

        $this->fields['FULL_NAME_EMPLOYEE']['pattern'] = '^[а-яА-Я. a-zA-Z0-9()]{3,255}$';
        $this->fields['FULL_NAME_EMPLOYEE']['errortext'] = ' Имя должно быть больше 2-х символов русскими буквами';
        $this->fields['FULL_NAME_EMPLOYEE']['type'] = 'text';
        $this->fields['FULL_NAME_EMPLOYEE']['required'] = true;

        $this->fields['BIRTHDATE']['pattern'] = '^.+$';
        $this->fields['BIRTHDATE']['errortext'] = ' Поле обязательно';
        $this->fields['BIRTHDATE']['type'] = 'date';
        $this->fields['BIRTHDATE']['required'] = true;

        $this->fields['UL_NAME']['pattern'] = '^.+$';
        $this->fields['UL_NAME']['errortext'] = ' Заполните поле';
        $this->fields['UL_NAME']['required'] = true;

        $this->fields['OP_NAME']['pattern'] = '^.+$';
        $this->fields['OP_NAME']['errortext'] = ' Заполните поле';
        $this->fields['OP_NAME']['required'] = true;

        $this->fields['PHONE']['pattern'] = '[\+\(\)\d- ]$';
        $this->fields['PHONE']['errortext'] = ' Номер не может содержать буквы';
        $this->fields['PHONE']['type'] = 'text';
        $this->fields['PHONE']['required'] = true;

        $this->fields['CERT_FORM']['pattern'] = '^.+$';
        $this->fields['CERT_FORM']['errortext'] = ' Заполните поле';
        $this->fields['CERT_FORM']['required'] = true;
        $this->fields['CERT_FORM']['DEPENDENCE'][Constants::CERT_FORM_PAPER] = array('DELIVERY_METHOD');
        $this->fields['CERT_FORM']['DEPENDENCE'][Constants::CERT_FORM_ELECTRONIC] = array('EMAIL');

        $this->fields['DELIVERY_METHOD']['pattern'] = '^.+$';
        $this->fields['DELIVERY_METHOD']['errortext'] = ' Заполните поле';
        $this->fields['DELIVERY_METHOD']['DEPENDENCE'][Constants::DELIVERY_METHOD_MAIL] = array('DELIVERY_ADDRESS', 'POSTCODE');

        $this->fields['DELIVERY_ADDRESS']['pattern'] = '^.+$';
        $this->fields['DELIVERY_ADDRESS']['errortext'] = ' Поле не должно быть пустым';
        $this->fields['DELIVERY_ADDRESS']['type'] = 'text';

        $this->fields['POSTCODE']['pattern'] = '^[\d+]{6}$';
        $this->fields['POSTCODE']['errortext'] = ' Индекс должен содержать 6 цифр';
        $this->fields['POSTCODE']['type'] = 'text';

        $this->fields['EMAIL']['pattern'] = '[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+';
        $this->fields['EMAIL']['errortext'] = ' Email должен быть вида example@address.domain';
        $this->fields['EMAIL']['type'] = 'text';

        $this->fields['DATE_START_PERIOD']['pattern'] = '^.+$';
        $this->fields['DATE_START_PERIOD']['errortext'] = ' Поле обязательно';
        $this->fields['DATE_START_PERIOD']['type'] = 'date';

        $this->fields['DATE_END_PERIOD']['pattern'] = '^.+$';
        $this->fields['DATE_END_PERIOD']['errortext'] = ' Поле обязательно';
        $this->fields['DATE_END_PERIOD']['type'] = 'date';

        $this->fields['FULL_NAME_CHILD']['pattern'] = '^[а-яА-Я]{3,255}$';
        $this->fields['FULL_NAME_CHILD']['errortext'] = ' Имя должно быть больше 2-х символов';
        $this->fields['FULL_NAME_CHILD']['type'] = 'text';

        $this->fields['BIRTHDATE_CHILD']['pattern'] = '^.+$';
        $this->fields['BIRTHDATE_CHILD']['errortext'] = ' Поле обязательно';
        $this->fields['BIRTHDATE_CHILD']['type'] = 'date';

        return $this;
    }

    public function getFields(): array {
        return $this->fields;
    }

    private function addInputSource($source): void {
        $this->fields['INPUT_SOURCE']['value'] = $source;
    }

    private function addEmployeeInfo($id): void {
        $filter = array('ID' => $id);
        $parameters = array(
            'SELECT' => array('UF_DEPARTMENT'),
            'FIELDS' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_BIRTHDAY', 'PERSONAL_MOBILE', 'EMAIL')
        );
        $result = CUser::GetList(($by = 'timestamp_x'), ($order = 'desc'), $filter, $parameters);

        $idDepartment = '';
        if ($user = $result->Fetch()) {
            $this->fields['FULL_NAME_EMPLOYEE']['value'] = $user['LAST_NAME'] . ' ' . $user['NAME'] . ' ' . $user['SECOND_NAME'] . ' (' . $user['ID'] . ')';
            $this->fields['BIRTHDATE']['value'] = date('Y-m-d', MakeTimeStamp($user['PERSONAL_BIRTHDAY'], 'DD.MM.YYYY'));
            $this->fields['PHONE']['value'] = $user['PERSONAL_MOBILE'];
            $this->fields['EMAIL']['value'] = $user['EMAIL'];
            $idDepartment = $user['UF_DEPARTMENT'][0];
        }

        $structure = new Structure(Constants::SECTION_ID_VELESSTROY);
        $structure->searchDepartments($idDepartment);

        $this->fields['UL_NAME']['value'] = $structure->legalEntity;
        $this->fields['OP_NAME']['value'] = $structure->separateSubdivision;
    }

    private function addBlockedFlags(): void {
        $this->fields['BIRTHDATE']['blocked'] = isset($this->fields['BIRTHDATE']['value']);
        $this->fields['FULL_NAME_EMPLOYEE']['blocked'] = isset($this->fields['FULL_NAME_EMPLOYEE']['value']);
        $this->fields['UL_NAME']['blocked'] = isset($this->fields['UL_NAME']['value']);
        $this->fields['OP_NAME']['blocked'] = isset($this->fields['OP_NAME']['value']);
        $this->fields['PHONE']['blocked'] = isset($this->fields['PHONE']['value']);
        $this->fields['EMAIL']['blocked'] = isset($this->fields['EMAIL']['value']);
    }
}

class CIntervolgaUniwinFormCreateRequest extends CBitrixComponent implements Controllerable {
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    
    public function executeComponent() {
        Extension::load('ui.vue');
        Extension::load('ui.vue.vuex');
        Extension::load('ui.bootstrap4');
        Extension::load('ui.fontawesome4');
        
        $this->includeComponentTemplate();
    }
    
    public function configureActions() {
        return array(
            'getArResult' => array(
                'prefilters' => array(),
            ),
            'sendForm' => array(
                'prefilters' => array(),
            ),
        );
    }
    
    // Результат работы компонента в данном случае получаю с помощью ajax.
    // А так по сути в переменной $result лежит то, что обычно лежит в $arResult.
    public function getArResultAction() {
        global $USER;
        $result = new ResultBuilder();
        $result->createForUser(new UserInfo($USER))
            ->withFieldsFromBp(Constants::BP)
            ->withStructureCompany(new Structure(Constants::SECTION_ID_VELESSTROY))
            ->withValidateData();
        
        return $result->getFields();
    }
}